import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolveComponent } from 'vue'
import path from 'path';

const baseVal = "/manager/"


export default ({command,mode}) =>{

  console.log("mode",mode)
  
  // 本地环境
  if(mode === "localdev"){
    console.log("mode localdev!")

    return {
      Val: baseVal,
      plugins: [vue()],
      resolve:{
        alias:{
          '@':path.resolve(__dirname,"src")
        }
      },
      server:{
        proxy:{
          "/api":{
            target:"http://localhost:8083",
            changeOrigin:true
          }
        }
      }
    }
  }

  // 测试环境
  else if (mode === "development"){
    console.log("mode development!")

    return {
      base: baseVal,

      plugins: [vue()],
      resolve:{
        alias:{
          '@':path.resolve(__dirname,"src")
        }
      }
    }
  }

  // 生产
  return {
    base: baseVal,
    plugins: [vue()],
    resolve:{
      alias:{
        '@':path.resolve(__dirname,"src")
      }
    }
  }



}



// https://vitejs.dev/config/
