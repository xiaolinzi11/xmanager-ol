import router from '@/router/index'
import {
    getJWTToken,
    removeJWTToken,
  } from '@/utils/tokens'
import store from '@/store/index'


const whiteList = ['/pages/login', '/auth-redirect']

// import app from "@/main.ts"

router.beforeEach((to: any, from: any, next: any) => {

    // console.log("from",from)
    // console.log("to",to)
    // console.log("vue",vue)

    if(to.path.name != "Login"){
      console.log("to.query",to.query)
      let tabsInfo = {
        path: to.path,
        query: to.query,
        title:to.name,
      }
      store.commit("managercore/tabs/PathChange", to.path);
      store.commit("managercore/tabs/AddTabsInfo", tabsInfo);
    }


    // NProgress.start(); // start progress bar
  
    if (getJWTToken()) {
      console.log("check JWT success")

      if (to.path === '/pages/login') {
        // NProgress.done();
        next({
          path: 'dashboard'
        });
      } 
      else if (to.path === '/'){

        next({
          path: 'dashboard'
        });
      }
      else{
        // console.log(3333)
        next();
      }

    } else {
      console.log("没有本地缓存");
  
      if (whiteList.indexOf(to.path) !== -1) { // 在免登录白名单，直接进入
        next()
      } else {
        next(`/pages/login?redirect=${to.path}`) // 否则全部重定向到登录页
        // NProgress.done() // if current page is login will not trigger afterEach hook, so manually handle it
      }
    }
  })
  
  