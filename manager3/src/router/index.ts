//  manager-core
import vue from '@vitejs/plugin-vue'
import { createRouter, createWebHistory,Router,RouteRecordRaw } from 'vue-router'

const TestPage = () => import('@/manager-core/page/TestPage.vue')
const TestPage2 = () => import('@/manager-core/page/TestPage2.vue')

const LoginPage = () => import('@/manager-core/page/login.vue')
const MainContainer = () => import('@/manager-core/page/container/MainContainer.vue')
const SubDefaultContainer = () => import('@/manager-core/page/container/SubDefaultContainer.vue')

const dashboard = () => import('@/page/dashboard.vue')

// muser
const muser = () => import('@/manager-core/page/muser/muser.vue')

// demo
const demoPage = () => import('@/manager-core/page/demo/demo.vue')
const DemoAdd = () => import('@/manager-core/page/demo/demoadd.vue')
const DemoEdit = () => import('@/manager-core/page/demo/demoedit.vue')

// permission
const permission = () => import('@/manager-core/page/permission/permission.vue')
const permissionAdd = () => import('@/manager-core/page/permission/permissionadd.vue')
const permissionEdit = () => import('@/manager-core/page/permission/permissionedit.vue')

// dict
const dic = () => import('@/manager-core/page/dic/dic.vue')
const dicadd = () => import('@/manager-core/page/dic/dicadd.vue')
const dicEdit = () => import('@/manager-core/page/dic/dicedit.vue')


// dicttype
const dictype = () => import('@/manager-core/page/dictype/dictype.vue')
const dictypeadd = () => import('@/manager-core/page/dictype/dictypeadd.vue')
const dictypeedit = () => import('@/manager-core/page/dictype/dictypeedit.vue')

// module
const modulePage = () => import('@/manager-core/page/module/module.vue')
const moduleAdd = () => import('@/manager-core/page/module/moduleadd.vue')
const moduleEdit = () => import('@/manager-core/page/module/moduleedit.vue')
const moduleLocation = () => import('@/manager-core/page/module/modulelocation.vue')

// role
const rolePage = () => import('@/manager-core/page/role/role.vue')
const roleadd = () => import('@/manager-core/page/role/roleadd.vue')
const roleEdit = () => import('@/manager-core/page/role/roleedit.vue')

// account
const accountPage = () => import('@/manager-core/page/account/account.vue')

// views
const TableView = () => import('@/manager-core/component/view/TableView.vue')
const FormView = () => import('@/manager-core/component/view/FormView.vue')
const SortView = () => import('@/manager-core/component/view/SortView.vue')


const routes:Array<RouteRecordRaw> = [
    {
        path: '/',
        name:'主页',
        redirect: to => {
            return {
                path : '/dashboard'
            }
        },
        component: MainContainer,
        children: [
            {
                path: 'dashboard',
                name: '概览',
                component: dashboard
            },

            // 组件
            {
                path: 'views',
                name: '组件',
                component: SubDefaultContainer,
                children: [
                    {
                        path: 'tablep',
                        name: '表格',
                        component: SubDefaultContainer,
                        redirect: to => {
                            return {
                                path : '/views/tablep/dashboard'
                            }
                        },
                        children: [
                            {
                                path: 'dashboard',
                                name: '表格组件概览',
                                component: TableView
                            },
// FormView
                            {
                                path: 'add',
                                name: '表单(增加)',
                                component: FormView
                            },

                            {
                                path: 'edit',
                                name: '表单(修改)',
                                component: FormView
                            }


                        ]
                    },
                    {
                        path: 'formp',
                        name: '表单',
                        component: FormView,
                    },
                    {
                        path: 'sortp',
                        name: '排序',
                        component: SortView,
                    }

                ]
            },
            
            // 系统管理 
            {
                path : 'system',
                name: '系统管理',
                component: SubDefaultContainer,

                children: [
                    {
                        path: 'muser',
                        name: '后台用户',
                        component: SubDefaultContainer,
                        redirect: to => {
                            return {
                                path : '/system/muser/dashboard'
                            }
                        },
                        children: [
                            {
                                path: 'dashboard',
                                name: '后台用户概览',
                                component: muser
                            }
                        ]
                    },

                    // demo
                    {
                        path: 'demo',
                        name: 'demo',
                        component: SubDefaultContainer,
                        redirect: to => {
                            return {
                                path : '/system/demo/dashboard'
                            }
                        },
                        children: [
                            {
                                path: 'dashboard',
                                name: 'demo概览',
                                component: demoPage
                            },
                            {
                                path: 'add',
                                name: '添加demo',
                                component: DemoAdd
                            },
                            {
                                path: 'edit',
                                name: '修改demo',
                                component: DemoEdit
                            }
                        ]
                    },

                    // permission
                    {
                        path: 'permission',
                        name: 'permission',
                        component: SubDefaultContainer,
                        redirect: to => {
                            return {
                                path : '/system/permission/dashboard'
                            }
                        },
                        children: [
                            {
                                path: 'dashboard',
                                name: '许可概览',
                                component: permission
                            },
                            {
                                path: 'add',
                                name: '添加许可',
                                component: permissionAdd
                            },
                            {
                                path: 'edit',
                                name: '修改许可',
                                component: permissionEdit
                            }
                        ]
                    },

                    // dic
                    {
                        path: 't-dic',
                        name: '字典',
                        component: SubDefaultContainer,
                        redirect: to => {
                            return {
                                path : '/system/t-dic/dashboard'
                            }
                        },
                        children: [
                            {
                                path: 'dashboard',
                                name: '字典概览',
                                component: dic
                            },
                            {
                                path: 'add',
                                name: '添加字典',
                                component: dicadd
                            },
                            {
                                path: 'edit',
                                name: '修改字典',
                                component: dicEdit
                            }
                        ]
                    },

                    // dictype
                    {
                        path: 't-dictype',
                        name: '字典类型',
                        component: SubDefaultContainer,
                        redirect: to => {
                            return {
                                path : '/system/t-dictype/dashboard'
                            }
                        },
                        children: [
                            {
                                path: 'dashboard',
                                name: '字典类型概览',
                                component: dictype
                            },
                            {
                                path: 'add',
                                name: '添加字典类型',
                                component: dictypeadd
                            },
                            {
                                path: 'edit',
                                name: '修改字典类型',
                                component: dictypeedit
                            }
                        ]
                    },
                    
                    // module
                    {
                        path: 'module',
                        name: '菜单',
                        component: SubDefaultContainer,
                        redirect: to => {
                            return {
                                path : '/system/module/dashboard'
                            }
                        },
                        children: [
                            {
                                path: 'dashboard',
                                name: '菜单概览',
                                component: modulePage
                            },
                            {
                                path: 'add',
                                name: '添加菜单',
                                component: moduleAdd
                            },
                            {
                                path: 'edit',
                                name: '修改菜单',
                                component: moduleEdit
                            },
                            {
                                path: 'location',
                                name: '调整菜单位置',
                                component: moduleLocation
                            }
                        ]
                    },

                    // role
                    {
                        path: 'role',
                        name: '角色',
                        component: SubDefaultContainer,
                        redirect: to => {
                            return {
                                path : '/system/role/dashboard'
                            }
                        },
                        children: [
                            {
                                path: 'dashboard',
                                name: '角色概览',
                                component: rolePage
                            },
                            {
                                path: 'add',
                                name: '添加角色',
                                component: roleadd
                            },
                            {
                                path: 'edit',
                                name: '修改角色',
                                component: roleEdit
                            }
                        ]
                    },

                    // account
                    {
                        path: 'account',
                        name: '账号管理',
                        component: SubDefaultContainer,
                        redirect: to => {
                            return {
                                path : '/system/account/dashboard'
                            }
                        },
                        children: [
                            {
                                path: 'dashboard',
                                name: '账号管理',
                                component: accountPage
                            }
                        ]
                    },
                    
                ]
            }
     
        ]        
    },



    {
        path: '/pages',
        name: 'pages',
        component:  LoginPage,
        children: [
            {
                path: 'login',
                name: 'Login',
                component: LoginPage
            }
        ] 
    }


]

const router:Router = createRouter({    
    history: createWebHistory("manager"),
    routes
})

export default router