import { loginApiMD,loginOutApiMD } from "./data/loginMockData";
import { getListApiMD } from "./data/tableMockData";
import { successMD } from "./data/commonMockData";
import { demoInitMD } from "./data/demoMockData";
import { locationApiMD } from "./data/sortViewMockData";





class MockDispatcher{

    getData(symbol:string){
        if(symbol == "loginApi"){
            return loginApiMD;
        }

        if(symbol == "logoutApi"){
            return loginOutApiMD;
        }

        if(symbol == "getListApi-/api/manager/t-demo"){
            return getListApiMD;
        }

        if(symbol == "demoEditInit"){
            return demoInitMD;
        }

        if(symbol == "SortViewlocationApi"){
            return locationApiMD;
        }

        if(symbol == "success"){
            return successMD;
        }



        return successMD
    }

}

let mockDispatcher = new MockDispatcher();
export default mockDispatcher;





