import Mock from 'mockjs';

let loginApiMD = Mock.mock({
    "user":{
      "rolename":"管理员",
      "company":"",
      "isAdmin":1,
      "authorities":[],
      "enabled":true,
      "username":"admin",
      "mUser": {
        "id":1,
        "account":"admin",
        "username":"admin",
        "phone":null,
        "email":null,
        "cdate":"2021-07-13T06:18:37.000+00:00",
        "status":0,
        "isadmin":1,
        "lastLoginDate":"2021-09-13T03:17:38.000+00:00"
      }
    },
    "menus":[
      {"data":{"id":1,"pid":null,"name":"首页","icon":"el-icon-odometer","url":"/","hasChild":false,"sort":-999,"cdate":null,"status":0},"children":null},
      {
        "data":{"id":6,"pid":null,"name":"页面管理","icon":"el-icon-box","url":"","hasChild":false,"sort":999,"cdate":null,"status":0},
        "children":[
            {"data":{"id":61,"pid":6,"name":"表格","icon":"","url":"/views/tablep","hasChild":false,"sort":9991,"cdate":null,"status":0},"children":null},
            {"data":{"id":62,"pid":6,"name":"表单","icon":"","url":"/views/formp","hasChild":false,"sort":9992,"cdate":null,"status":0},"children":null},
            {"data":{"id":63,"pid":6,"name":"排序","icon":"","url":"/views/sortp","hasChild":false,"sort":9993,"cdate":null,"status":0},"children":null},            
        ]
      },
      {
        "data":{"id":2,"pid":null,"name":"系统管理","icon":"el-icon-setting","url":"","hasChild":false,"sort":999,"cdate":null,"status":0},
        "children":[
            {"data":{"id":3,"pid":2,"name":"角色","icon":"","url":"/system/role","hasChild":false,"sort":9991,"cdate":null,"status":0},"children":null},
            {"data":{"id":4,"pid":2,"name":"后台用户","icon":"","url":"/system/muser","hasChild":false,"sort":9992,"cdate":null,"status":0},"children":null}
        ]
      }
    ]
})

let loginOutApiMD = Mock.mock(
    {
        "data":{
          "success":true
        }
    }
)

export {
    loginApiMD,
    loginOutApiMD
}