import Mock from 'mockjs';

let getListApiMD = Mock.mock({
  "current":1,
  "total":5,
  "hitCount":false,
  "pages":1,
  "optimizeCountSql":true,
  "size":10,
  "maxLimit":null,
  "records":[
    {"pdate":"1923年8月","publisher":"北京新潮社出版","name":"呐喊","writer":"鲁迅","id":1,"status":0},
    {"pdate":"1926年8月","publisher":"北京北新书局","name":"彷徨","writer":"鲁迅","id":2,"status":0},
    {"pdate":"1935年","publisher":"上海文化生活出版社","name":"故事新编","writer":"鲁迅","id":3,"status":0},
    {"pdate":"1928年9月","publisher":"北平未名社","name":"朝花夕拾","writer":"鲁迅","id":4,"status":0},
    {"pdate":"1927年7月","publisher":"北京新潮社","name":"野草","writer":"鲁迅","id":5,"status":1}
  ],
  "searchCount":true,
  "orders":[],
  "countId":null
})



export {
  getListApiMD
}