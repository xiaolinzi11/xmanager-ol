import Mock from 'mockjs';

let locationApiMD = Mock.mock([
  {"id":1,"title":"朝花夕拾","cover":"/static/manager3/demo/book1.jpg","sort":5},
  {"id":2,"title":"彷徨","cover":"/static/manager3/demo/book2.jpg","sort":4},
  {"id":3,"title":"呐喊","cover":"/static/manager3/demo/book3.jpg","sort":3},
  {"id":4,"title":"阿Q正传","cover":"/static/manager3/demo/book4.jpg","sort":2},
  {"id":5,"title":"狂人日记","cover":"/static/manager3/demo/book5.jpg","sort":1}]
)


export {
  locationApiMD
}