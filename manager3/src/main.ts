import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus';
// import 'element-plus/lib/theme-chalk/index.css';
import 'element-plus/dist/index.css'

import locale from 'element-plus/lib/locale/lang/zh-cn'

import '@/router/permission'
import {registerGlobalComponent} from '@/manager-core/config'

import globalData from '@/config';

const app = createApp(App)


app.config.globalProperties.$globalData = globalData



// 对vue进行类型补充说明
declare module '@vue/runtime-core' {
    interface ComponentCustomProperties {
        $globalData:any,
    }
}


app.use(router);
app.use(ElementPlus,{locale});
app.use(store);

registerGlobalComponent(app);

app.mount('#app')


