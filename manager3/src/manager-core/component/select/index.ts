import BaseSelect from "./BaseSelect.vue"
import ModuleSelect from "./ModuleSelect.vue"
import DefaultStatusSelect from "./DefaultStatusSelect.vue"
import PermissionTypeSelect from "./PermissionTypeSelect.vue"
import DictTypeSelect from "./DictTypeSelect.vue"
import RoleSelect from "./RoleSelect.vue"

export {
    ModuleSelect,
    BaseSelect,
    DefaultStatusSelect,
    PermissionTypeSelect,
    DictTypeSelect,
    RoleSelect
}