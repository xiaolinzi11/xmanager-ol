import request from '@/api/request'
import { Base64 } from "js-base64"

import qs from 'qs';

import globalData from '@/config';
import mockDispatcher from "@/mock/MockDispatcher"

import {
  getJWTToken,removeJWTToken,setJWTToken
} from "@/utils/tokens"


// var Mock = require('mockjs')
// const Random = require('mockjs').Random;


export function loginApi(username: string, password: string) {
  const serverUrl = "/api/manager/login";

  if(globalData.offline){
    return new Promise((resolve,reject)=>{
      let d = mockDispatcher.getData("loginApi");

      // 保存本地缓存
      setJWTToken("token11");
      
      resolve(d);
    })  
  }

  const data = {
    "username": username,
    "password": Base64.encode(password)
  }
  return request({
    url: serverUrl,
    method: 'post',
    data : qs.stringify(data)    
  })
  

}

export function logoutApi() {
  const serverUrl = "/api/manager/logout";

  if(globalData.offline){
    return new Promise((resolve,reject)=>{
      let d = mockDispatcher.getData("logoutApi");

      removeJWTToken()
      resolve(d);
    })  
  }


  return request({
    url: serverUrl,
    method: 'post'
  })
}

// 获取详细用户信息
export function getUserInfoApi() {
  const serverUrl = "/api/manager/userinfo";

  return request({
    url: serverUrl,
    method: 'get'
  })
}

export function checkServer() {

  const serverUrl = "/api/manager/checkserver";

  return request({
    url: serverUrl,
    method: 'get'
  })
}



// const whiteSites = ["account", "pages"];
// const webManagerSites = ["workplatform", "topic", "directtrain",
//   "banner", "book", "sportsvideo", "culture", "question","shsports"
// ];
// const contentSites = ["article", ];
// const systemManagerSites = ["company", "muser", "role"];
// const reviewSites = ["review"];
// const dataSites = ["stat"];

export function checkPermission(to: { path: string }) {
  // let roleInfo: MyRoleInfo = store.getters["profileModule/roleinfos"];
  // if(roleInfo.isadmin){
  //   return true;
  // }
  // if (!to || to.path === '/' || to.path === '/dashboard')
  //   store.commit("profileModule/DelTabsInfo", [
  //     {
  //       path: "/dashboard",
  //       query: {},
  //       title: "首页",
  //     },
  //   ]);
  return true;

  // if(roleInfo.hasPer0001){
  //   console.log("has permission manager")
  // }

}