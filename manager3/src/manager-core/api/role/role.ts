//后台用户相关 api
import request from '@/api/request'


export function saveApi(data: any) {
    let url = '/api/manager/role/save';
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function editBasicApi(id: any) {
    let url = '/api/manager/role/edit/init/basic';

    let data = {
        id: id
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

export function updateBasicApi(data: any) {
    let url = '/api/manager/role/edit/save/basic';
    return request({
        url: url,
        method: 'post',
        data
    })
}


// 获取 修改页面数据
export function optionsApi(data: any) {
    let url = '/api/manager/role/options';
    return request({
        url: url,
        method: 'post',
        data
    })
}
