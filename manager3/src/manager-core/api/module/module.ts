//后台用户相关 api
import request from '@/api/request'



// 获取 修改页面数据
export function moduleEditInitApi(id: any) {
    let url = '/api/manager/module/edit/init';
    let data = {
        "id": id
    }
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function moduleEditSaveApi(data: any) {
    let url = '/api/manager/module/edit/save';
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function moduleLocationApi() {
    let url = '/api/manager/module/location';

    let data = {
        "test" : 1
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

export function moduleLocationSubmitAllApi(data:any) {
    let url = '/api/manager/module/location/submitall';

    return request({
        url: url,
        method: 'post',
        data
    })
}


export function moduleLocationSaveApi(data:any) {
    let url = '/api/manager/module/location/save';

    return request({
        url: url,
        method: 'post',
        data
    })
}




// 增加文章
export function moduleAddApi(data: any) {
    let url = '/api/manager/module/save';
    return request({
        url: url,
        method: 'post',
        data
    })
}



export function moduleOptionsApi(data: any) {
    let url = '/api/manager/module/options';
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function moduleGetApi() {
    let url = '/api/manager/module/get';

    let data = {
        "test" : 1
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

