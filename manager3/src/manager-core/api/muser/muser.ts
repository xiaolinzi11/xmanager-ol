//后台用户相关 api
import request from '@/api/request'
import {Base64} from "js-base64";



// 获取 修改页面数据
export function muserEditInitApi(id: string) {
    let url = '/api/manager/muser/edit/init';
    let data = {
        "id": id
    }
    return request({
        url: url,
        method: 'post',
        data
    })
}

// 增加文章
export function muserAddApi(data: any) {
    let password = data.password;
    let enp = Base64.encode(password)
    data.password = enp;

    let url = '/api/manager/muser/save';
    return request({
        url: url,
        method: 'post',
        data
    })
}
// 修改用户密码
export function muserPasswordChangeApi(data: any) {
    let password = data.p;
    let enp = Base64.encode(password)
    data.p = enp;

    let url = '/api/manager/muser/edit/pwd';
    return request({
        url: url,
        method: 'post',
        data
    })
}


// 修改用户名
export function muserEditUsernameApi(data: any) {
    let url = '/api/manager/muser/edit/save';
    return request({
        url: url,
        method: 'post',
        data
    })
}

// 更新用户密码
export function muserEditPasswordApi(data: any) {
    let url = '/api/manager/muser/edit/pwd';
    return request({
        url: url,
        method: 'post',
        data
    })
}

// 修改权限信息
export function muserEditRoleApi(data: any) {
    let url = '/api/manager/muser/edit/role';
    return request({
        url: url,
        method: 'post',
        data
    })
}

// 修改后台用户的绑定手机号
export function muserEditPhoneApi(data: any) {
    let url = '/api/manager/muser/updatep';
    return request({
        url: url,
        method: 'post',
        data
    })
}


