import request from '@/api/request'


export function listApi(id:number) {

    let url = "/api/manager/role-permission/list"

    let data = {
        rid: id,
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

export function addApi(rid:number,pid:number) {

    let url = "/api/manager/role-permission/add"

    let data = {
        rid: rid,
        pid: pid
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

export function delApi(id:number) {

    let url = "/api/manager/role-permission/del"

    let data = {
        id: id,
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

