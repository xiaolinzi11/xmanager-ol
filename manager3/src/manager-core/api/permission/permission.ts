import request from '@/api/request'

export function permissionAddApi(data: any) {
    let url = "/api/manager/permission/save";
    return request({
        url,
        method: 'post',
        data
    })
}
export function getPermissionInitApi(id:string){
    let url = '/api/manager/permission/edit/init';
    return request({
        url,
        method:'post',
        data:{
            id
        }
    })
}
export function permissionEditSaveApi(data: any) {
    let url = "/api/manager/permission/edit/save";
    return request({
        url,
        method: 'post',
        data
    })
}