//后台用户相关 api
import request from '@/api/request'



// 获取 修改页面数据
export function dicEditInitApi(id: any) {
    let url = '/api/manager/t-dic/edit/init';
    let data = {
        "id": id
    }
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function dicEditSaveApi(data: any) {
    let url = '/api/manager/t-dic/edit/save';
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function dicLocationApi() {
    let url = '/api/manager/t-dic/location';

    let data = {
        "test" : 1
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

export function dicLocationSubmitAllApi(data:any) {
    let url = '/api/manager/t-dic/location/submitall';

    return request({
        url: url,
        method: 'post',
        data
    })
}


export function dicLocationSaveApi(data:any) {
    let url = '/api/manager/t-dic/location/save';

    return request({
        url: url,
        method: 'post',
        data
    })
}




// 增加文章
export function dicAddApi(data: any) {
    let url = '/api/manager/t-dic/save';
    return request({
        url: url,
        method: 'post',
        data
    })
}



export function dicOptionsApi(data: any) {
    let url = '/api/manager/t-dic/options';
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function dicGetApi() {
    let url = '/api/manager/t-dic/get';

    let data = {
        "test" : 1
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

