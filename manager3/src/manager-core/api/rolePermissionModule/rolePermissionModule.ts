import request from '@/api/request'



export function listByRidApi(rid:any) {

    let url = "/api/manager/role-permission-module/listbyrid"

    let data = {
        rid: rid
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

//
//  
//
export function updatePermissionApi(id:number,rid:number,check:boolean) {

    let url = "/api/manager/role-permission-module/updatePermission"

    let data = {
        id: id,
        rid: rid,
        check:check
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}




