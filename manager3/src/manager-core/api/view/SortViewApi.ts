//后台用户相关 api
import request from '@/api/request'

import globalData from '@/config';
import mockDispatcher from "@/mock/MockDispatcher"

export function locationApi(serverUrl: string | undefined, datamap?: any) {
    let url = serverUrl + "/location/init";

    if(globalData.offline){
        return new Promise((resolve,reject)=>{
          let d = mockDispatcher.getData("SortViewlocationApi");    
          resolve(d);
        })  
    }



    let data: any = {
        "default": 1
    };

    if (datamap) {
        datamap.forEach((value: any, key: any) => {
            data[key] = value;
        });
    }


    return request({
        url: url,
        method: 'post',
        data
    })
}