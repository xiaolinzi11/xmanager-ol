// table 数据相关
import request from '@/api/request'
import mockDispatcher from "@/mock/MockDispatcher"
import globalData from '@/config';

/*  
    serverUrl : 基础的 api 地址
    tableFilterData ： 过滤数据
        {
            require(
                status、searchword
            )
        }
    tablePageData ： 分页数据
*/
export function getListApi(serverUrl: string | undefined
    , tableFilterData: any
    , tablePageData: { start: number; size: number; current: number }, sid?: string, pid?: string) {


    if(globalData.offline){

        let symbol = "getListApi-" + serverUrl;

        console.log("mock table data list : " + symbol)


        return new Promise((resolve,reject)=>{
            let d = mockDispatcher.getData(symbol);

            
            // 保存本地缓存            
            resolve(d);
        })  
    }


    let url = serverUrl + "/list";
    let data: any = {
        "status": tableFilterData.status,
        "searchprop": tableFilterData.searchProp,
        "searchword": tableFilterData.searchword,
        "current": tablePageData.current,
        "start": tablePageData.start,
        "size": tablePageData.size,
        sid,
        pid,
    }

    if (tableFilterData) {
        if (tableFilterData.status != undefined) {
            data.status = tableFilterData.status;
        }
        if (tableFilterData.cid != undefined) {
            data.cid = tableFilterData.cid;
        }
        if (tableFilterData.searchword != undefined) {
            data.searchword = tableFilterData.searchword;
        }

        if (tableFilterData.sid != undefined) {
            data.sid = tableFilterData.sid;
        }
        if (tableFilterData.pid != undefined) {
            data.pid = tableFilterData.pid;
        }
        if (tableFilterData.partyid != undefined) {
            data.partyid = tableFilterData.partyid;
        }
        if (tableFilterData.unitid != undefined) {
            data.unitid = tableFilterData.unitid;
        }
        if (tableFilterData.stype != undefined) {
            data.stype = tableFilterData.stype;
        }
        // 第四步
        if (tableFilterData.areaId != undefined) {
            data.areaId = tableFilterData.areaId;
        }
        if (tableFilterData.cityId != undefined) {
            data.cityId = tableFilterData.cityId;
        }

        if (tableFilterData.type != undefined) {
            data.type = tableFilterData.type;
        } if (tableFilterData.key != undefined) {
            data.key = tableFilterData.key;
        }
        if (tableFilterData.informationId != undefined) {
            data.informationId = tableFilterData.informationId;
        }

        // datamap
        if (tableFilterData.dataMap) {
            let map = tableFilterData.dataMap;

            if(map.size > 0){
                map.forEach((value: any, key: any) => {
                    data[key] = value;
    
                    // console.log("data put",key,value)
                });
            }
        }

    }

    return request({
        url: url,
        method: 'post',
        data
    })
}



/*  
    serverUrl : 基础的 api 地址
    id : data id
*/
export function stopApi(serverUrl: string | undefined
    , id: string | number | undefined) {
    let url = serverUrl + "/stop";
    // let url = "/muser/stop";

    let data = {
        "id": id
    }


    return request({
        url: url,
        method: 'post',
        data
    })
}

/*  
    serverUrl : 基础的 api 地址
    id : data id
*/
export function startApi(serverUrl: string | undefined
    , id: string | number | undefined) {
    let url = serverUrl + "/start";

    let data = {
        "id": id
    }


    return request({
        url: url,
        method: 'post',
        data
    })
}

/*  
    serverUrl : 基础的 api 地址
    id : data id
*/
export function deleteApi(serverUrl: string | undefined
    , id: string | number | undefined) {
    let url = serverUrl + "/delete";

    let data = {
        "id": id
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

export function updateStatusApi(serverUrl: string | undefined
    , id: any, status:any) {
    let url = serverUrl ;

    if(status == 0){
        url = url + "/start"
    }
    else if(status == 1){
        url = url + "/stop";
    }
    else if(status == 2){
        url = url + "/delete";
    }

    let data = {
        "id": id
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}