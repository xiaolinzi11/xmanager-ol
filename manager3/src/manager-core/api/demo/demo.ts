//后台用户相关 api
import request from '@/api/request'

import globalData from '@/config';
import mockDispatcher from "@/mock/MockDispatcher"

// 获取 修改页面数据
export function demoEditInitApi(id: any) {

    if(globalData.offline){
        return new Promise((resolve,reject)=>{
          let d = mockDispatcher.getData("demoEditInit");    
          resolve(d);
        })  
    }



    let url = '/api/manager/t-demo/edit/init';
    let data = {
        "id": id
    }
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function demoEditSaveApi(data: any) {    
    if(globalData.offline){
        return new Promise((resolve,reject)=>{
          let d = mockDispatcher.getData("success");    
          resolve(d);
        })  
    }

    let url = '/api/manager/t-demo/edit/save';
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function demoLocationApi() {
    let url = '/api/manager/t-demo/location';

    let data = {
        "test" : 1
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

export function demoLocationSubmitAllApi(data:any) {
    let url = '/api/manager/t-demo/location/submitall';

    return request({
        url: url,
        method: 'post',
        data
    })
}


export function demoLocationSaveApi(data:any) {
    let url = '/api/manager/t-demo/location/save';

    return request({
        url: url,
        method: 'post',
        data
    })
}




// 增加文章
export function demoAddApi(data: any) {

    if(globalData.offline){
        return new Promise((resolve,reject)=>{
          let d = mockDispatcher.getData("success");    
          resolve(d);
        })  
      }


    let url = '/api/manager/t-demo/save';
    return request({
        url: url,
        method: 'post',
        data
    })
}



export function demoOptionsApi(data: any) {
    let url = '/api/manager/t-demo/options';
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function demoGetApi() {
    let url = '/api/manager/t-demo/get';

    let data = {
        "test" : 1
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

