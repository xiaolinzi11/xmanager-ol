//后台用户相关 api
import request from '@/api/request'



// 获取 修改页面数据
export function dictypeEditInitApi(id: any) {
    let url = '/api/manager/t-dictype/edit/init';
    let data = {
        "id": id
    }
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function dictypeEditSaveApi(data: any) {
    let url = '/api/manager/t-dictype/edit/save';
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function dictypeLocationApi() {
    let url = '/api/manager/t-dictype/location';

    let data = {
        "test" : 1
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

export function dictypeLocationSubmitAllApi(data:any) {
    let url = '/api/manager/t-dictype/location/submitall';

    return request({
        url: url,
        method: 'post',
        data
    })
}


export function dictypeLocationSaveApi(data:any) {
    let url = '/api/manager/t-dictype/location/save';

    return request({
        url: url,
        method: 'post',
        data
    })
}




// 增加文章
export function dictypeAddApi(data: any) {
    let url = '/api/manager/t-dictype/save';
    return request({
        url: url,
        method: 'post',
        data
    })
}



export function dictypeOptionsApi(data: any) {
    let url = '/api/manager/t-dictype/options';
    return request({
        url: url,
        method: 'post',
        data
    })
}

export function dictypeGetApi() {
    let url = '/api/manager/t-dictype/get';

    let data = {
        "test" : 1
    }

    return request({
        url: url,
        method: 'post',
        data
    })
}

