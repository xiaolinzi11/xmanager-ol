import {MenuGroup,MenuItem} from "@/manager-core/page/container/impl"
import {rolePermissionModuleListCheckbox} from "@/manager-core/page/role/impl"


/**
 * @param app 
 * 
 * 注册全局组件
 * 
 */
export function registerGlobalComponent(app:any){
    app.component('MenuGroup',MenuGroup);
    app.component('MenuItem',MenuItem); 
    app.component('rolePermissionModuleListCheckbox',rolePermissionModuleListCheckbox); 
}



