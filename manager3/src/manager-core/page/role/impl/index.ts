import roleBasicInput  from './roleBasicInput.vue';
import roleModuleInput  from './roleModuleInput.vue';
import rolePermissionModuleListCheckbox  from './rolePermissionModuleListCheckbox.vue';
import rolePerInput  from './rolePerInput.vue';

export {
    roleBasicInput,
    roleModuleInput,
    rolePerInput,
    rolePermissionModuleListCheckbox
}