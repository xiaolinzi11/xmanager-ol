const myMixin = {
  data() {
    return {
        DefaultColor : '#FFFFFF',
        PrimaryColor : '#409EFF',
        SuccessColor : "#67C23A",
        WarningColor : "#E6A23C",
        DangerColor : "#F56C6C",
        InfoColor : "#909399",

        TextPrimary : "#303133",
        TextPrimary2 : "#606266",
        TextPrimary3 : "#909399",
        TextPrimary4 : "#C0C4CC",

        BorderPrimary : "#DCDFE6",
        BorderPrimary2 : "#E4E7ED",
        BorderPrimary3 : "#EBEEF5",
        BorderPrimary4 : "#F2F6FC"
    }
  }
}

export default myMixin;
