import profiles from './profiles'
import menus from './menu'
import tabs from './tabs'


export default {
  namespaced: true,
  modules:{
    profiles,
    menus,
    tabs
  }
}