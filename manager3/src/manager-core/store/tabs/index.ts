const state = {
  tabsInfo:[],
  activePath: ""
}

// getters
const getters = {
  activePath: (state:any, getters:any, rootState:any) => {
    return state.activePath;
  },
  tabsInfo: (state:any, getters:any, rootState:any) => {
    return state.tabsInfo;
  }
}

// actions
const actions = {
  clear ({ state, commit }:any) {
    commit("clear");
  }
}

// mutations
const mutations = {
  AddTabsInfo (state:any, payload:any) {
    let tabsInfo = state.tabsInfo as any;
    // console.log("AddTabsInfo tabsInfo",tabsInfo,payload)
    // console.log("AddTabsInfo tabsInfo.length",tabsInfo.length)

    if (tabsInfo.length > 0) {
      if(payload.path=="/pages/login"||!payload.title){
        return;
      }
      for (let i = 0; i < tabsInfo.length; i++) {
        if (tabsInfo[i].title == payload.title) {
          return;
        }
      }
      state.tabsInfo.push(payload)
    } 
    else {
      state.tabsInfo = [];
      state.tabsInfo.push(payload)
    }
  },

  DelTabsInfo (state:any, payload:any) {
    console.log("DelTabsInfo",payload)
    state.tabsInfo = payload;
  },

  PathChange(state:any, payload:any) {
    state.activePath = payload;
  },

  clear (state:any) {
    state.tabsInfo = [];
    state.activePath = "";
  },

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}