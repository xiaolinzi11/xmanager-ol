import { createStore, createLogger } from 'vuex'
import managercore from "../manager-core/store"
import createPersistedState from 'vuex-persistedstate'

// import products from './modules/products'

export default createStore({
    modules: {
        managercore
    },
    plugins: [createPersistedState()]
})