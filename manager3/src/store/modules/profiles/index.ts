
// initial state
// shape: [{ id, quantity }]
const state = () => ({
  id : "",
  username: "",
  isadmin: "",
  rolename: "",
  auths: []
})

// getters
const getters = {
  username: (state:any, getters:any, rootState:any) => {
    return state.username;
  },

  isadmin: (state:any, getters:any, rootState:any) => {
    return state.isadmin;
  },

  rolename: (state:any, getters:any, rootState:any) => {
    return state.rolename;
  },

  id: (state:any, getters:any, rootState:any) => {
    return state.id;
  },

  auths: (state:any, getters:any, rootState:any) => {
    return state.auths;
  },
}

// actions
const actions = {

  LoginByUsername ({ state, commit }:any, userObj:any) {
    console.log("userObj",userObj);
  }
}

// mutations
const mutations = {
  setUser (state:any, payload:any) {
    state.id = payload.id;
    state.username = payload.username;
    state.isadmin = payload.isadmin;
    state.rolename = payload.rolename;
    state.auths = payload.auths;
  },

  clear (state:any) {
    state.id = "";
    state.username = "";
    state.isadmin = "";
    state.rolename = "";
    state.auths = [];
  },

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}