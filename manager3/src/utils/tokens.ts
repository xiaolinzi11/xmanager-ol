import Cookies from 'js-cookie'

const TokenKey:string = 'Admin-Token'
const JWTTokenKey:string = 'jwt-Token'


export function setJWTToken(token:string) {
  return Cookies.set(JWTTokenKey, token)
}

export function getJWTToken() {
  return Cookies.get(JWTTokenKey)
}

export function removeJWTToken() {
  return Cookies.remove(JWTTokenKey)
}


