## XMANAGER 是什么

XMANAGER 是一款后台管理系统框架，前端基于 VUE3.0 ， ElementUI-plus 进行实现。 本项目遵循灵活、轻量化的开发理念，尽量减少理解成本，从而实现快速使用与落地。

## 文档
[查看文档](https://adea.yuque.com/books/share/e9667db8-cabe-4c83-ad1d-d677db5344ef)

## 特点与优势

- 轻量化
- 亲和的UI与UE
- 基于 VUE3.0 与 ElementUI-Plus
- 丰富的落地案例

## 预览与落地案例展示

<table>
    <tr>
        <td><img src = "https://images.gitee.com/uploads/images/2021/0910/162359_f515d749_94653.jpeg"/></td>
        <td><img src = "https://images.gitee.com/uploads/images/2021/0910/153809_d3b9b046_94653.png"/></td>
    </tr>
    <tr>
        <td><img src = "https://images.gitee.com/uploads/images/2021/0910/153741_484d6309_94653.png"/></td>
        <td><img src = "https://images.gitee.com/uploads/images/2021/0910/153751_6a9f40a0_94653.png "/></td>
    </tr>
    <tr>
        <td><img src = "https://images.gitee.com/uploads/images/2021/0910/153741_484d6309_94653.png"/></td>
        <td><img src = "https://images.gitee.com/uploads/images/2021/0910/153751_6a9f40a0_94653.png "/></td>
    </tr>
</table>



## 开源共建
 _感谢所有参与开源共建的同学！_ 
<p><a href="https://gitee.com/alan59?utm_source=poper_profile"><img src = "https://images.gitee.com/uploads/images/2021/0910/164046_5c05ecba_94653.jpeg" width="60",height="60"/></a>
 <a href="https://gitee.com/in_a_relaxed_manner"><img src = "https://images.gitee.com/uploads/images/2021/0910/164420_e9b84281_94653.jpeg" width="60",height="60"/></a> 
<a href="https://gitee.com/zhihao1204"><img src = "https://images.gitee.com/uploads/images/2021/0910/164456_b92e4c8a_94653.jpeg" width="60",height="60"/></a> 
<a href="https://gitee.com/asfca"><img src = "https://images.gitee.com/uploads/images/2021/0910/164548_aa99effb_94653.jpeg" width="60",height="60"/></a>
<a href="https://gitee.com/zhghzx"><img src = "https://images.gitee.com/uploads/images/2021/0910/164702_b3ed57b9_94653.jpeg" width="60",height="60"/></a>
</p>